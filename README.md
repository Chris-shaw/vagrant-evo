# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick Setup of Development Enviroment

### Requirements ###
* Virtualbox `apt install virtualbox`
* Vagrant `apt install vagrant`
* Vagrant VBGuest Addon - `vagrant plugin install vagrant-vbguest`

### How do I get set up? ###

* Create Directory /home/{USER}/Projects/
* Create Directory /home/{USER}/vagrant/
* Clone Repository into /home/{USER}/vagrant/vagrant-evo
* CD Into  /home/{USER}/vagrant/vagrant-evo
* Run `vagrant up`